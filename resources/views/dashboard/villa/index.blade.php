@extends('dashboard.layouts.master')

@section('title', '| All Villa')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All Villa</h1>
		</div>

		<div class="col-md-2">
			<a href="{{ route('admin.villa.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Create New Villa</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Thumbnails</th>
					<th>Name</th>
					<th>Desc</th>
					<th>Type</th>
					<th>Beds</th>
					<th>Bath</th>
					<th>Price</th>
					<th>Location</th>
					<th>View</th>
					<th></th>
				</thead>

				<tbody>

					@foreach ($villas as $villa)

						<tr>
							<th>{{ $villa->id }}</th>
							{{-- <td><img src="/storage/uploads/{{ $villa->image }}" alt="" style="height:70px; width:150px;"></td> --}}
							<td><img src="{{route('getuploadsvilla', $villa->image)}}" alt="" style="height:70px; width:150px;"></td>
							<td>{{ $villa->name }}</td>
							<td>{{ substr(strip_tags($villa->desc), 0, 50) }}{{ strlen(strip_tags($villa->desc)) > 50 ? "..." : "" }}</td>
							<td>{{ $villa->type }}</td>
							<td>{{ $villa->beds }}</td>
							<td>{{ $villa->baths }}</td>
							<td>{{ $villa->price }}</td>
							<td>{{ $villa->location }}</td>
							<td>{{ $villa->view }}</td>
							{{-- <td>{{ date('M j, Y', strtotime($villa->created_at)) }}</td> --}}
							<td>
								<div class="row">
<div class="col-sm-6 text-right">
								<a  href="{{ route('admin.villa.edit', $villa->id) }}" class="btn btn-default btn-sm">Edit</a>
							</div>
								<div class="col-sm-6">
						{!! Form::open(['route' => ['admin.villa.destroy', $villa->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}

						{!! Form::close() !!}
					</div>
								</div>
							</td>
						</tr>

					@endforeach

				</tbody>
			</table>

			<div class="text-center">
				{!! $villas->links(); !!}
			</div>
		</div>
	</div>

@stop
