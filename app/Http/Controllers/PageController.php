<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Category;
use App\Fileentry;
use App\Post;
use App\Villa;
use App\Customer;
use Carbon\Carbon;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // variable dari semua tipe villa
        $posts = Post::all();
        // passing variable ke view
        return view('page.special-events')->withPosts($posts);
    }

    public function villas()
    {
        // variable dari semua tipe villa
        $villas = Villa::all();
        // passing variable ke view
        return view('page.villas')->withVillas($villas);
    }

    public function getVillaFilter(Request $r) {
        $villas = Villa::all();

        if($r->villatype != 'All') {

            $villas = $villas->where('type', $r->villatype);
        }
         
        if($r->price == "1") {
            $villas = $villas->filter(function ($item) {
                $a = str_replace('.', '', $item['price']);

                return $a <= 2000000;
                });
        }elseif ($r->price == "2") {
            $villas = $villas->filter(function ($item) {
                $a = str_replace('.', '', $item['price']);

                return ($a > 2000000 && $a <= 5000000);
                });
        }elseif ($r->price == "3") {
            $villas = $villas->filter(function ($item) {
                $a = str_replace('.', '', $item['price']);

                return $a > 5000000;

            });
        }
         if($r->location != 'All') {

            $villas = $villas->where('location', $r->location);
        }
         if($r->view != 'All') {

            $villas = $villas->where('view', $r->view);
        }

        return view('page.villas')->withVillas($villas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('page.special-events-show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bookNow($slug)
    {
        $villa = Villa::where('slug', $slug)->first();
        return view('page.book-now', compact('villa'));
    }

    public function bookNowSubmit(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'check_in' => 'required',
            'check_out' => 'required',
            'villa_id' => 'required',                        
            'notes' => 'required'
        ]);

        $villa = Villa::where('id', $request->villa_id)->first();

        $customer = new Customer();
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->villa_id = $request->villa_id;
        $customer->check_in = Carbon::createFromFormat('d/m/Y', $request->check_in);
        $customer->check_out = Carbon::createFromFormat('d/m/Y', $request->check_out);
        $customer->price = $villa->price;
        $customer->notes = $request->notes;
        $customer->save();

        return redirect()->route('home.index')->with('success', 'Anda berhasil booking');
    }
}
