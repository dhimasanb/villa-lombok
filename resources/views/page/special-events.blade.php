@extends('page.layouts.master')

@section('title')
  Special Events
@endsection

@section('content')
<section id="content">
    <div class="container_12">
      <div class="grid_8">
        @if ($posts->isEmpty())
          <div class="">
            <h2 class="top-1 p3"></h2>
            <div class="wrap">
                {{-- <img src="" alt="" class="img-border img-indent" style="height:100%; width:40%;"> --}}
                <div class="extra-wrap">
                    <p class="color-1 p6">Sorry there's no upcoming events yet</p>
                    <p></p>
                </div>
            </div>
            <p class="p5"></p>
            {{-- <a href="" class="button">Read more</a> --}}
          </div>
        @else
          @foreach ($posts as $post)
            <div class="">
              <h2 class="top-1 p3">{{ $post->category->name }}</h2>
              <div class="wrap">
                  <img src="{{ route('getuploads', $post->image)}}" alt="" class="img-border img-indent" style="height:200%; width:80%;">
                  <div class="extra-wrap">
                      {{-- <p class="color-1 p6">{{ $post->title }}</p> --}}
                      {{-- <p>{{ substr(strip_tags($post->content), 0, 150) }}{{ strlen(strip_tags($post->content)) > 150 ? "..." : "" }}</p> --}}
                  </div>
              </div>
              {{-- <p class="p5">{{ substr(strip_tags($post->content), 150, 300) }}{{ strlen(strip_tags($post->content)) > 300 ? "..." : "" }}</p> --}}
              <p class="color-1 p6">{{ $post->title }}</p>
              <p class="p5">{!! $post->content !!}</p>
              <br>
              <a href="{{ route('events.show', $post->id) }}" class="button">Read more</a>
            </div>
          @endforeach
        @endif
      </div>
      <div class="grid_4">
</section>
@endsection
