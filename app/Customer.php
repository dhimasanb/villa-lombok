<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';

    public function villa()
    {
        return $this->belongsTo('App\Villa');
    }


}
