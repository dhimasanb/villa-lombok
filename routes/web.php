<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return view('page.index');
})->name('home.index');


Route::get('/special-events', 'PageController@index')->name('events.index');
Route::get('/special-events/{id}', 'PageController@show')->name('events.show');
Route::get('/villas', 'PageController@villas')->name('villa.index');
Route::get('/book-now/{slug}', 'PageController@bookNow')->name('book-now.index');
Route::post('/book-now', 'PageController@bookNowSubmit')->name('book-now.submit');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('uploads/{filename}', ['as' => 'getuploads', 'uses' => 'PostController@getUploads']);
Route::get('uploads/villa/{filename}', ['as' => 'getuploadsvilla', 'uses' => 'VillaController@getUploads']);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
  Route::post('/post', 'PostController@store')->name('admin.post.store');
  Route::get('/post', 'PostController@index')->name('admin.post.index');
  Route::get('/post/create', 'PostController@create')->name('admin.post.create');
  Route::delete('/post/{id}', 'PostController@destroy')->name('admin.post.destroy');
  Route::put('/post/{id}', 'PostController@update')->name('admin.post.update');
  Route::get('/post/{id}', 'PostController@show')->name('admin.post.show');
  // Route::get('/post/{slug}', 'PostController@showSlug')->name('admin.post.show.slug')->where('slug', '[\w\d\-\_]+');
  Route::get('/post/{id}/edit', 'PostController@edit')->name('admin.post.edit');

  // Category
  Route::post('/category', 'CategoryController@store')->name('admin.category.store');
  Route::get('/category', 'CategoryController@index')->name('admin.category.index');
  Route::get('/category/create', 'CategoryController@create')->name('admin.category.create');
  Route::delete('/category/{id}', 'CategoryController@destroy')->name('admin.category.destroy');
  Route::put('/category/{id}', 'CategoryController@update')->name('admin.category.update');
  Route::get('/category/{id}', 'CategoryController@show')->name('admin.category.show');
  Route::get('/category/{id}/edit', 'CategoryController@edit')->name('admin.category.edit');

  // Tags
  Route::post('/tags', 'TagController@store')->name('admin.tags.store');
  Route::get('/tags', 'TagController@index')->name('admin.tags.index');
  Route::get('/tags/create', 'TagController@create')->name('admin.tags.create');
  Route::delete('/tags/{id}', 'TagController@destroy')->name('admin.tags.destroy');
  Route::put('/tags/{id}', 'TagController@update')->name('admin.tags.update');
  Route::get('/tags/{id}', 'TagController@show')->name('admin.tags.show');
  Route::get('/tags/{id}/edit', 'TagController@edit')->name('admin.tags.edit');

  // Villa
  Route::post('/villa', 'VillaController@store')->name('admin.villa.store');
  Route::get('/villa', 'VillaController@index')->name('admin.villa.index');
  Route::get('/villa/create', 'VillaController@create')->name('admin.villa.create');
  Route::delete('/villa/{id}', 'VillaController@destroy')->name('admin.villa.destroy');
  Route::put('/villa/{id}', 'VillaController@update')->name('admin.villa.update');
  Route::get('/villa/{id}', 'VillaController@show')->name('admin.villa.show');
  // Route::get('/villa/{slug}', 'VillaController@showSlug')->name('admin.villa.show.slug')->where('slug', '[\w\d\-\_]+');
  Route::get('/villa/{id}/edit', 'VillaController@edit')->name('admin.villa.edit');

  // Customer
  Route::post('/customer', 'CustomerController@store')->name('admin.customer.store');
  Route::get('/customer', 'CustomerController@index')->name('admin.customer.index');
  Route::get('/customer/create', 'CustomerController@create')->name('admin.customer.create');
  Route::delete('/customer/{id}', 'CustomerController@destroy')->name('admin.customer.destroy');
  Route::put('/customer/{id}', 'CustomerController@update')->name('admin.customer.update');
  Route::get('/customer/{id}', 'CustomerController@show')->name('admin.customer.show');
  // Route::get('/villa/{slug}', 'VillaController@showSlug')->name('admin.villa.show.slug')->where('slug', '[\w\d\-\_]+');
  Route::get('/customer/{id}/edit', 'CustomerController@edit')->name('admin.customer.edit');
});
