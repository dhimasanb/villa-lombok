@extends('page.layouts.master')

@section('title')
  {{ $post->title }}
@endsection

@section('content')
<section id="content">
    <div class="container_12">
      <div class="grid_8">
        <div class="">
          <h2 class="top-1 p3">{{ $post->category->name }}</h2>
          <div class="wrap">
              <img src="{{ route('getuploads', $post->image)}}" alt="" class="img-border img-indent" style="height:100%; width:40%;">
              <div class="extra-wrap">
                  {{-- <p class="color-1 p6">{{ $post->title }}</p> --}}
                  {{-- <p>{{ substr(strip_tags($post->content), 0, 150) }}{{ strlen(strip_tags($post->content)) > 150 ? "..." : "" }}</p> --}}
              </div>
          </div>
          {{-- <p class="p5">{{ substr(strip_tags($post->content), 150, 300) }}{{ strlen(strip_tags($post->content)) > 300 ? "..." : "" }}</p> --}}
          <p class="color-1 p6">{{ $post->title }}</p>
          <p class="p5">{!! $post->content !!}</p>
          <br>
        </div>
      </div>
      <div class="grid_4">

            </form>
            <h2 class="p3">Our Contacts</h2>
                   <dl>
                <dt class="color-1 p2"><strong>Majapahit Road,<br>Mataram City.</strong></dt>
                <dd><span>Telephone:</span>(0370) 636 001</dd>
                <dd><span>E-mail:</span><a href="#" class="link">madinalombok@gmail.com</a></dd>
            </dl>
        </div>
      </div>
      <div class="clear"></div>
    </div>
</section>
@endsection
