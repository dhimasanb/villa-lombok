@extends('dashboard.layouts.master')

@section('title', '| Create New Post')

@section('stylesheets')

	{!! Html::style('css/parsley.min.css') !!}
	{!! Html::style('css/select2.min.css') !!}
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

	<script>
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: false
		});
	</script>

@endsection

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Create New Villa</h1>
			<hr>
			{!! Form::model($villa, array('route' => ['admin.villa.update', $villa->id], 'data-parsley-validate' => '', 'files' => true, 'method' => 'PUT')) !!}
				{{ Form::label('name', 'Name:') }}
				{{ Form::text('name', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255', 'placeholder' => 'Put the Villas name here')) }}

				{{ Form::label('slug', 'Slug:') }}
				{{ Form::text('slug', null, array('class' => 'form-control', 'placeholder' => 'Put the link here') ) }}

				{{ Form::label('type', 'Type:') }}
				<select class="form-control" name="type">
					<option disabled selected>Please select...</option>
					<option value="Private Villa">Private Villa</option>
					<option value="Bungalows">Bungalows</option>
					<option value="Resort">Resort</option>
				</select>

				{{ Form::label('beds', 'Beds:') }}
				<select class="form-control" name="beds">
					<option disabled selected>Please select...</option>
						<option value='1 BR'>1 BR</option>
						<option value='2 BR'>2 BR</option>
						<option value='3 BR'>3 BR</option>
						<option value='4 BR'>4 BR</option>
						<option value='5 BR'>5 BR</option>
						<option value='6 BR'>6 BR</option>
				</select>

				{{ Form::label('baths', 'Baths:') }}
				<select class="form-control" name="baths">
					<option disabled selected>Please select...</option>
						<option value='1'>1</option>
						<option value='2'>2</option>
						<option value='3'>3</option>
						<option value='4'>4</option>
						<option value='5'>5</option>
						<option value='6'>6</option>
				</select>

				{{ Form::label('price', 'Price:') }}
				{{ Form::text('price', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255', 'placeholder' => 'Example: 1.500.000')) }}

				{{ Form::label('location', 'Location:') }}
				<select class="form-control" name="location">
					<option disabled selected>Please select...</option>
					<option value="West Lombok">West Lombok</option>
					<option value="Center Lombok">Center Lombok</option>
					<option value="East Lombok">East Lombok</option>
					<option value="Gili Island">Gili Island</option>
				</select>

				{{ Form::label('view', 'View:') }}
				<select class="form-control" name="view">
					<option disabled selected>Please select...</option>
					<option value="Ocean View">Ocean View</option>
					<option value="Garden View">Garden View</option>
					<option value="Mountain View">Mountain View</option>
					<option value="Field View">Field View</option>
				</select>

				{{ Form::label('featured_img', 'Upload a Featured Image') }}
				{{ Form::file('featured_img') }}

				{{ Form::label('desc', "Description:") }}
				{{ Form::textarea('desc', null, array('class' => 'form-control')) }}

				{{ Form::submit('Create Villa', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}
		</div>
	</div>

@endsection


@section('scripts')

	{!! Html::script('js/parsley.min.js') !!}
	{!! Html::script('js/select2.min.js') !!}

	<script type="text/javascript">
		$('.select2-multi').select2();
	</script>

@endsection
