<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
// use Request;
use Illuminate\Http\Response;
use App\Tag;
use App\Category;
use App\Fileentry;
use Session;
use Purifier;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // variable dari semua model customer
        $customers = Customer::orderBy('id', 'desc')->paginate(10);
        // passing variable ke view
        return view('dashboard.customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi data
        $this->validate($request, array(
                'name'            => 'required|max:255',
                //'slug'          => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                //'category_id'   => 'required|integer',
                'desc'            => 'required',
                'price'           => 'required',
            ));

        // store in the database
        $customer = new Customer;
        $customer->name        = $request->name;
        if ($request->has('slug')) {
          $customer->slug = str_slug($request->slug);
        } else {
          $customer->slug = str_slug($customer->name);
        }
        $customer->desc        = Purifier::clean($request->desc);
        $customer->type        = $request->type;
        $customer->price       = $request->price;


        // Public Folder
        /*if ($request->hasFile('featured_img')) {
          $image = $request->file('featured_img');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          // $location = public_path('images/' . $filename);
          $location = Storage::disk('local')->put('public/uploads/' . $filename);

          Image::make($image)->resize(800, 400)->save($location);
          $post->image = $filename;
        }*/


        $customer->save();

        // flash messages
        $request->session()->flash('status', 'Thank you, we will give e-ticket to your e-mail soon!');

        // redirect ke halaman
        return redirect()->route('admin.customer.index', $customer->id);
    }

    public function getUploads($filename)
    {

		$customer = Customer::where('image', '=', $filename)->firstOrFail();
		$file = Storage::disk('local')->get('public/uploads/'.$customer->image);

    return (new Response($file, 200))
              ->header('Content-Type', $customer->mime);
	  }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mahasiswa  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        return view('dashboard.customer.show', compact('customer'));
    }

    public function showSlug($slug)
    {
        //fetch from the DB based on slug
        $customer = Customer::where('slug', '=',  $slug)->first();
        return view('dashboard.customer.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mahasiswa  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the post in the database and save as a var
        $customer = Customer::findOrFail($id);
        // return the view and pass in the var we previously created
        return view('dashboard.customer.edit')->withCustomer($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mahasiswa  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // Validate the data
       

     $this->validate($request, array(
                'name'            => 'required|max:255',
                //'slug'          => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                //'category_id'   => 'required|integer',
                'desc'            => 'required',
                'type'            => 'required',
                'price'           => 'required',
            ));

        // Save the data to the database
        $customer = Customer::find($id);
        $customer->name        = $request->name;
        $customer->email        = $request->email;
        $customer->save();

     
        // set flash data with success message
        //Session::flash('success', 'This post was successfully saved.');
        // redirect with flash data to posts.show
        return redirect()->route('admin.customer.index', $customer->id)->with('status','Data berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mahasiswa  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->tags()->detach();
        $customer->delete();
        //Session::flash('success', 'The post was successfully deleted.');
        return redirect()->route('admin.customer.index')->with('status','Data berhasil dihapus.');
    }
}
