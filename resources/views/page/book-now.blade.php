@extends('dashboard.layouts.contactdetails')

@section('title', '| Book Now')

@section('stylesheets')

    {!! Html::style('css/parsley.min.css') !!}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

     {!! Html::style('css/datepicker.css') !!}
      {!! Html::style('css/jquery-ui.min.css') !!}

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>

    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'link code',
            menubar: false
        });
    </script>

@endsection

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Fill Contact Details</h1>
            <hr>
            {!! Form::open(array('route' => 'book-now.submit', 'data-parsley-validate' => '', 'files' => true)) !!}
                {{ Form::label('name', 'Name:') }}
                {{ Form::text('name', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255', 'placeholder' => 'Put your name here')) }}

                {{ Form::label('phone', 'Phone Number:') }}
                {{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Put your number here') ) }}


                {{ Form::label('e-mail', 'E-mail:') }}
                {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Put your email here') ) }}

                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <label class="control-label" for="from">Villa</label>
                            <input id="villa_id" type="hidden" class="form-control" name="villa_id" value="{{ $villa->id }}">
                           <input type="text" class="form-control" value="{{ $villa->name }}" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group">

                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label class="control-label" for="from">Check In</label>
     
                        <input data-toggle="datepicker" class="form-control from" size="11" 
                        value="<?php echo (isset($_SESSION['from'])) ? $_SESSION['from'] : ''; ?>" 
                        data-date="" data-date-format="yyyy-mm-dd" data-link-field="any" 
                        data-link-format="yyyy-mm-dd" type="text" value=""  name="check_in" id="from">
                       
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <label class="control-label" for="to">Check Out</label>
                        
                            <input data-toggle="datepicker" class="form-control to" size="11" type="text" value="<?php echo (isset($_SESSION['to'])) ? $_SESSION['to'] : ''; ?>"  name="check_out" id="to" data-date="" data-date-format="yyyy-mm-dd" data-link-field="any" data-link-format="yyyy-mm-dd">
                           
                    </div>
                </div>
            </div>  
                

                {{ Form::label('desc', "Notes:") }}
                {{ Form::textarea('notes', null, array('class' => 'form-control')) }}

                {{ Form::submit('Book Now', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
            {!! Form::close() !!}
        </div>
    </div>

@endsection


@section('scripts')

    {!! Html::script('js/parsley.min.js') !!}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script type="text/javascript">
        
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });


           jQuery(function() {
              calender();
              calenderTo();
              });

        function calender(){
          $( '#from' ).datepicker()
            }

                    function calenderTo(){
          $( '#to' ).datepicker()
            }
   

        
    </script>

    <script type="text/javascript">
        $('.select2-multi').select2();
    </script>



@endsection
