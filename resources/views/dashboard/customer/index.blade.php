@extends('dashboard.layouts.master')

@section('title', '| All Customer')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All Customer</h1>
		</div>

		
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Villa</th>
					<th>Price</th>
					<th>Check-in</th>
					<th>Check-out</th>
					<th>Notes</th>
					<th>Created At</th>
				</thead>

				<tbody>

					@foreach ($customers as $customer)

						<tr>
							<th>{{ $customer->id }}</th>
							<td>{{ $customer->name }}</td>
							<td>{{ $customer->phone }}</td>
							<td>{{ $customer->email }}</td>
							<td>{{ $customer->villa->name }}</td>
							<td>{{ $customer->villa->price }}</td>
							<td>{{ $customer->check_in }}</td>
							<td>{{ $customer->check_out }}</td>
							<td>{!! $customer->notes !!}</td>
							{{-- <td>{{ date('M j, Y', strtotime($customer->created_at)) }}</td> --}}
							<td><a  href="{{ route('admin.customer.edit', $customer->id) }}" class="btn btn-default btn-sm">Edit</a></td>
						</tr>

					@endforeach

				</tbody>
			</table>

			<div class="text-center">
				{!! $customers->links(); !!}
			</div>
		</div>
	</div>

@stop
