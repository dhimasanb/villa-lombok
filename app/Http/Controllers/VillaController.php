<?php

namespace App\Http\Controllers;

use App\Villa;
use Illuminate\Http\Request;
// use Request;
use Illuminate\Http\Response;
use App\Tag;
use App\Category;
use App\Fileentry;
use Session;
use Purifier;
use Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class VillaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // variable dari semua model mahasiswa
        $villas = Villa::orderBy('id', 'desc')->paginate(10);
        // passing variable ke view
        return view('dashboard.villa.index')->withVillas($villas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.villa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi data
        $this->validate($request, array(
                'name'            => 'required|max:255',
                //'slug'          => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                //'category_id'   => 'required|integer',
                'desc'            => 'required',
                'type'            => 'required',
                'beds'            => 'required',
                'baths'            => 'required',
                'price'           => 'required',
                'location'        => 'required',
                'view'            => 'required',
            ));

        // store in the database
        $villa = new Villa;
        $villa->name        = $request->name;
        if ($request->has('slug')) {
          $villa->slug = str_slug($request->slug);
        } else {
          $villa->slug = str_slug($villa->name);
        }
        $villa->desc        = Purifier::clean($request->desc);
        $villa->type        = $request->type;
        $villa->beds        = $request->beds;
        $villa->baths       = $request->baths;
        $villa->price       = $request->price;
        $villa->location    = $request->location;
        $villa->view        = $request->view;


        // Public Folder
        /*if ($request->hasFile('featured_img')) {
          $image = $request->file('featured_img');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          // $location = public_path('images/' . $filename);
          $location = Storage::disk('local')->put('public/uploads/' . $filename);

          Image::make($image)->resize(800, 400)->save($location);
          $post->image = $filename;
        }*/

        // Storage Folder
        if ($request->hasFile('featured_img')) {
          $image = $request->file('featured_img');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          Storage::disk('local')->put('public/uploads/'.$image->getFilename().'.'.$filename,  File::get($image));
          $villa->mime = $image->getClientMimeType();
          $villa->original_filename = $image->getClientOriginalName();
          //Image::make($image)->resize(800, 400)->save($location);
          $villa->image = $image->getFilename().'.'.$filename;;
        }

        $villa->save();

        // flash messages
        $request->session()->flash('status', 'Data berhasil disimpan!');

        // redirect ke halaman
        return redirect()->route('admin.villa.index', $villa->id);
    }

    public function getUploads($filename)
    {

		$villa = Villa::where('image', '=', $filename)->firstOrFail();
		$file = Storage::disk('local')->get('public/uploads/'.$villa->image);

    return (new Response($file, 200))
              ->header('Content-Type', $villa->mime);
	  }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mahasiswa  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $villa = Villa::findOrFail($id);
        return view('dashboard.villa.show', compact('villa'));
    }

    public function showSlug($slug)
    {
        //fetch from the DB based on slug
        $villa = Villa::where('slug', '=',  $slug)->first();
        return view('dashboard.villa.show', compact('villa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mahasiswa  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the post in the database and save as a var
        $villa = Villa::findOrFail($id);
        $categories = Category::all();
        $cats = array();
        foreach ($categories as $category) {
            $cats[$category->id] = $category->name;
        }

        $tags = Tag::all();
        $tags2 = array();
        foreach ($tags as $tag) {
            $tags2[$tag->id] = $tag->name;
        }
        // return the view and pass in the var we previously created
        return view('dashboard.villa.edit')->withVilla($villa)->withCategories($cats)->withTags($tags2);
        //return view('dashboard.posts.edit', compact('post','cats','tags2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mahasiswa  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // Validate the data
       

     $this->validate($request, array(
                'name'            => 'required|max:255',
                //'slug'          => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                //'category_id'   => 'required|integer',
                'desc'            => 'required',
                'type'            => 'required',
                'beds'            => 'required',
                'baths'            => 'required',
                'price'           => 'required',
                'location'        => 'required',
                'view'            => 'required',
            ));

        // Save the data to the database
        $villa = Villa::find($id);
        $villa->name        = $request->name;
        if ($request->has('slug')) {
          $villa->slug = str_slug($request->slug);
        } else {
          $villa->slug = str_slug($villa->name);
        }
        $villa->desc        = Purifier::clean($request->desc);
        $villa->type        = $request->type;
        $villa->beds        = $request->beds;
        $villa->baths       = $request->baths;
        $villa->price       = $request->price;
        $villa->location    = $request->location;
        $villa->view        = $request->view;

        
        if ($request->hasFile('featured_img')) {
          $image = $request->file('featured_img');
          $filename = time() . '.' . $image->getClientOriginalExtension();
          Storage::disk('local')->put('public/uploads/'.$image->getFilename().'.'.$filename,  File::get($image));
          $villa->mime = $image->getClientMimeType();
          $villa->original_filename = $image->getClientOriginalName();
          //Image::make($image)->resize(800, 400)->save($location);
          $villa->image = $image->getFilename().'.'.$filename;;
        }


        $villa->save();

     
        // set flash data with success message
        //Session::flash('success', 'This post was successfully saved.');
        // redirect with flash data to posts.show
        return redirect()->route('admin.villa.index', $villa->id)->with('status','Data berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mahasiswa  $villa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $villa = Villa::findOrFail($id);
        $villa->delete();
        return redirect()->route('admin.villa.index')->with('success','Data berhasil dihapus.');
    }
}
