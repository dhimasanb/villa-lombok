<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fileentry extends Model
{
    protected $table = 'fileentries';

    public function posts()
    {
      return $this->belongsToMany('App\Post');
    }
}
