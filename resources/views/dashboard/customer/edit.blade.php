@extends('dashboard.layouts.master')

@section('title', '| Create New Post')

@section('stylesheets')

	{!! Html::style('css/parsley.min.css') !!}
	{!! Html::style('css/select2.min.css') !!}
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

	<script>
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: false
		});
	</script>

@endsection

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Edit Customer</h1>
			<hr>
			{!! Form::model($customer, array('route' => ['admin.customer.update', $customer->id], 'data-parsley-validate' => '', 'files' => true, 'method' => 'PUT')) !!}
				{{ Form::label('name', 'Name:') }}
				{{ Form::text('name', $customer->name, array('class' => 'form-control', 'required' => '', 'maxlength' => '255', 'placeholder' => 'Put the Villas name here')) }}

				{{ Form::label('email', "Email:") }}
				{{ Form::text('email', $customer->email, array('class' => 'form-control')) }}

				{{ Form::submit('Update Customer', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}
		</div>
	</div>

@endsection


@section('scripts')

	{!! Html::script('js/parsley.min.js') !!}
	{!! Html::script('js/select2.min.js') !!}

	<script type="text/javascript">
		$('.select2-multi').select2();
	</script>

@endsection
