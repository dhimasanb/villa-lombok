@extends('page.layouts.master')

@section('title')
  {{ $villa->name }}
@endsection

@section('content')
<section id="content">
    <div class="container_12">
      <div class="grid_8">
        <div class="">
          <div class="wrap">
              <img src="{{ route('getuploadsvilla', $villa->image)}}" alt="" class="img-border img-indent" style="height:100%; width:40%;">
              <div class="extra-wrap">
                  {{-- <p class="color-1 p6">{{ $villa->name }}</p> --}}
                  {{-- <p>{{ substr(strip_tags($villa->desc), 0, 150) }}{{ strlen(strip_tags($villa->description)) > 150 ? "..." : "" }}</p> --}}
              </div>
          </div>
          {{-- <p class="p5">{{ substr(strip_tags($villa->desc), 150, 300) }}{{ strlen(strip_tags($villa->desc)) > 300 ? "..." : "" }}</p> --}}
          <p class="color-1 p6">{{ $villa->name }}</p>
          <p class="p5">{!! $villa->desc !!}</p>
          <br>
        </div>
      </div>
      <div class="grid_4">
<!--==============================Tab_Filter==========================================================-->
      <div class="grid_4">
        <div class="left-1">
            <h2 class="top-1 p3">Find your villa</h2>
            <form id="form-1" class="form-1 bot-1">
                <div class="select-1">
                    <label>Villa type</label>
                    <select name="select" >
                        <option>Private Villa</option>
                        <option>Bungalows</option>
                        <option>Resort</option>
                    </select>
                    </div>
                <div class="select-2">
                    <label>Beds</label>
                    <select name="select" >
                        <option>1 BR</option>
                        <option>2 BR</option>
                        <option>Up to 2 BR</option>
                    </select>
                </div>
                <div class="select-2 last">
                    <label>Baths</label>
                    <select name="select" >
                        <option>1</option>
                        <option>2</option>
                    </select>
                </div>
                <div class="select-3">
                <label>Price</label>
                 <select name="select" >
                        <option>Under 2.000.000</option>
                        <option>Up to 2.000.000</option>
                        <option>Up to 5.000.000</option>
                    </select>
                </div>
                <div class="select-4">
                    <label>Location</label>
                    <select name="select" >
                        <option>West Lombok</option>
                        <option>Center Lombok</option>
                        <option>East Lombok</option>
                        <option>Gili Island</option>
                    </select>
                </div>
                <div class="select-5">
                    <label>View</label>
                    <select name="select" >
                        <option>Ocean View</option>
                        <option>Garden View</option>
                        <option>Mountain View</option>
                        <option>Field View</option>
                    </select>
                </div>
                <a onClick="document.getElementById('form-1').submit()" class="button">Search</a>
                <div class="clear"></div>
<!--==============================Add_Note==========================================================-->           
            </form>
            <h2 class="p3">Our Contacts</h2>
                   <dl>
                <dt class="color-1 p2"><strong>Majapahit Road,<br>Mataram City.</strong></dt>
                <dd><span>Telephone:</span>(0370) 636 001</dd>
                <dd><span>E-mail:</span><a href="#" class="link">madinalombok@gmail.com</a></dd>
            </dl>
        </div>
      </div>
      <div class="clear"></div>
    </div>
</section>
@endsection
