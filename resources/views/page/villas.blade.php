@extends('page.layouts.master')

@section('title')
  Villas
@endsection
<!--==============================Display_Product==========================================================-->
@section('content')
<section id="content">
    <div class="container_12">
      <div class="grid_8">
      @if ($villas->isEmpty())
          <div class="">
       <h2 class="top-1 p3"></h2>
        <div class="wrap">
          {{-- <img src="" alt="" class="img-border img-indent" style="height:100%; width:40%;"> --}}
            <div class="extra-wrap">
              <p class="color-1 p6">Can't find the result</p>
              <p></p>
                </div>
            </div>
                <p class="p5"></p>
            {{-- <a href="/contact-us" class="button">Book Now</a> --}}
          </div>
        @else
          @foreach ($villas as $villa)
            <div class="">
              <h2 class="top-1 p3">{{ $villa->name }}</h2>
              <div class="wrap">

                 <img src="{{ route('getuploadsvilla', $villa->image) }}" alt="" class="img-border img-indent" style="height:100%; width:40%;">
                  

                      {{-- <p class="color-1 p6">{{ $villa->type }}</p> --}}
                      
                      {{-- <p>{{ substr(strip_tags($villa->desc), 0, 150) }}{{ strlen(strip_tags($villa->desc)) > 150 ? "..." : "" }}</p> --}}
                  </div>
              </div>
              {{-- <p class="p5">{{ substr(strip_tags($villa->desc), 150, 300) }}{{ strlen(strip_tags($villa->desc)) > 300 ? "..." : "" }}</p> --}}
              <p class="color-1 p6">{{ $villa->type }}</p>
              <p>Price    : {{$villa->price}}</p>
              <p>Beds     : {{$villa->beds}}</p>
              <p>Baths    : {{$villa->baths}}</p>             
              <p>Location : {{$villa->location}}</p>
              <p>view     : {{$villa->view}}</p>
              <p class="p5">{!! $villa->desc !!}</p>
              <br>
              <a href="{{ route('book-now.index', $villa->slug)}}" class="button">Book Now</a>
              <a href="https://www.google.com/maps?q=Qunci+Villas+Lombok&hl=en&ll=-8.453356,116.093903&spn=0.226848,0.41851&geocode=+&hq=Qunci+Villas+Lombok&radius=15000&t=m&z=12" class="button">View Map</a>
            
          @endforeach
        @endif
        </div>
      </div>
      <div class="grid_4">
<!--==============================Tab_Filter==========================================================-->
      <div class="grid_4">
        <div class="right-1">
            <h2 class="top-1 p3">Find your villa</h2>
            <form id="form-1" class="form-1 bot-1" method="POST">
            {{csrf_field()}}
                <div class="select-1">
                    <label>Villa type</label>
                    <select name="villatype" >
                        <option value="All">All</option>
                        <option value="Private Villa">Private Villa</option>
                        <option value="Bungalows">Bungalows</option>
                        <option value="Resort">Resort</option>
                    </select>
                    </div>
                <div class="select-3">
                <label>Price</label>
                 <select name="price" >
                        <option value="All">All</option>
                        <option value="1">Under 2.000.000</option>
                        <option value="2">Up to 2.000.000</option>
                        <option value="3">Up to 5.000.000</option>
                    </select>
                </div>
                <div class="select-4">
                <label>Bed</label>
                 <select name="bed" >
                        <option value="All">All</option>
                        <option value="1">1 BR</option>
                        <option value="2">2 BR</option>
                        <option value="3">Up to 2 BR</option>
                    </select>
                </div>
                <div class="select-5">
                    <label>Location</label>
                    <select name="location" >
                        <option value="All">All</option>
                        <option value="West Lombok">West Lombok</option>
                        <option value="Center Lombok">Center Lombok</option>
                        <option value="East Lombok">East Lombok</option>
                        <option value="Gili Island">Gili Island</option>
                    </select>
                </div>
                <div class="select-6">
                    <label>View</label>
                    <select name="view" >
                        <option value="All">All</option>
                        <option value="Ocean View">Ocean View</option>
                        <option value="Garden View">Garden View</option>
                        <option value="Mountain View">Mountain View</option>
                        <option value="Field View">Field View</option>
                    </select>
                </div>
                <a onClick="document.getElementById('form-1').submit()" class="button">Search</a>
                <div class="clear"></div>
<!--==============================Add_Note==========================================================-->
            </form>
            <h2 class="p3">Our Contacts</h2>
             <dl>
                <dt class="color-1 p2"><strong>Majapahit Road,<br>Mataram City.</strong></dt>
                <dd><span>Telephone:</span>(0370) 636 001</dd>
                <dd><span>E-mail:</span><a href="#" class="link">madinalombok@gmail.com</a></dd>
            </dl>
        </div>
      </div>
      <div class="clear"></div>
    </div>
</section>
@endsection
